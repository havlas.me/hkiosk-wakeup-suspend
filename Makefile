.PHONY: install
install:
	install -d $(DESTDIR)/etc/default
	install -m 644 default/hkiosk-wakeup-suspend $(DESTDIR)/etc/default/
	install -d $(DESTDIR)/usr/lib/systemd/system
	install -m 644 systemd/hkiosk-wakeup-suspend.service $(DESTDIR)/usr/lib/systemd/system/
	install -d $(DESTDIR)/usr/libexec
	install -m 755 libexec/hkiosk-suspend $(DESTDIR)/usr/libexec/
	install -d $(DESTDIR)/usr/libexec/hkiosk-suspend.d
	install -m 755 libexec/hkiosk-wakeup $(DESTDIR)/usr/libexec/
	install -d $(DESTDIR)/usr/libexec/hkiosk-wakeup.d
	install -d $(DESTDIR)/usr/share/hkiosk-wakeup-suspend
	install -m 644 support/cecctl-suspend.sh $(DESTDIR)/usr/share/hkiosk-wakeup-suspend/
	install -m 644 support/cecctl-wakeup.sh $(DESTDIR)/usr/share/hkiosk-wakeup-suspend/
	install -m 644 support/vcgencmd-suspend.sh $(DESTDIR)/usr/share/hkiosk-wakeup-suspend/
	install -m 644 support/vcgencmd-wakeup.sh $(DESTDIR)/usr/share/hkiosk-wakeup-suspend/

.PHONY: uninstall
uninstall:
	-rm $(DESTDIR)/etc/default/hkiosk-wakeup-suspend
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-wakeup-suspend.service
	-rm $(DESTDIR)/usr/libexec/hkiosk-suspend
	-rmdir $(DESTDIR)/usr/libexec/hkiosk-suspend.d
	-rm $(DESTDIR)/usr/libexec/hkiosk-wakeup
	-rmdir $(DESTDIR)/usr/libexec/hkiosk-wakeup.d
	-rm $(DESTDIR)/usr/share/hkiosk-wakeup-suspend/cecctl-suspend.sh
	-rm $(DESTDIR)/usr/share/hkiosk-wakeup-suspend/cecctl-wakeup.sh
	-rm $(DESTDIR)/usr/share/hkiosk-wakeup-suspend/vcgencmd-suspend.sh
	-rm $(DESTDIR)/usr/share/hkiosk-wakeup-suspend/vcgencmd-wakeup.sh
	-rmdir $(DESTDIR)/usr/share/hkiosk-wakeup-suspend
