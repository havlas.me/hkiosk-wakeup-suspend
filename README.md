hKiosk Wakeup/Suspend Daemon
============================

[![MIT license][license-image]][license-link]

A simple systemd service that manages wakeup and suspend events bound to the
[`hkiosk-pmstate.service`](https://gitlab.com/havlas.me/hkiosk-pmstated). Custom actions can be bound to the wakeup
and suspend events by placing shell scripts in the `/usr/libexec/hkiosk-wakeup.d` and `/usr/libexec/hkiosk-suspend.d`
respectably, that are executed by the service in the alphabetical order.

Sample scripts are provided in the [`support`](/support) directory for `cecctl`, `cpufreq`, and `vcgencmd` utils.

Installation
------------

```shell
DESTDIR= make install
```

License
-------

[MIT][license-link]


[license-image]: https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square
[license-link]: LICENSE
