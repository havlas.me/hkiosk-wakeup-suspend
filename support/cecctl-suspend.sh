#!/bin/sh
# SPDX-License-Identifier: MIT
# Copyright (C) 2024, Tomáš Havlas <tomas@havlas.me>
# Distributed under the MIT License.
#
# This file is part of the hkiosk-wakeup-suspend project.
# https://gitlab.com/havlas.me/hkiosk-wakeup-suspend

CEC_SUPPORT="${CEC_SUPPORT:-1}"
CEC_OSD_NAME="${CEC_OSD_NAME:-hKiosk}"

[ "$CEC_SUPPORT" = "1" ] || exit 0

cec-ctl -s --playback --no-rc-passthrough --osd-name "$CEC_OSD_NAME" --cec-version-1.4

if cec-ctl -s --to 0 --give-device-power-status | grep -q 'pwr-state: on'; then
    cec-ctl -s --to 0 --standby
fi
