#!/bin/sh
# SPDX-License-Identifier: MIT
# Copyright (C) 2024, Tomáš Havlas <tomas@havlas.me>
# Distributed under the MIT License.
#
# This file is part of the hkiosk-wakeup-suspend project.
# https://gitlab.com/havlas.me/hkiosk-wakeup-suspend

if cpufreq-info -p | grep -q -v powersave; then
    cpufreq-set -g powersave
fi
