#!/bin/sh
# SPDX-License-Identifier: MIT
# Copyright (C) 2024, Tomáš Havlas <tomas@havlas.me>
# Distributed under the MIT License.
#
# This file is part of the hkiosk-wakeup-suspend project.
# https://gitlab.com/havlas.me/hkiosk-wakeup-suspend

if vcgencmd display_power | grep -q display_power=0; then
    vcgencmd display_power 1
fi
